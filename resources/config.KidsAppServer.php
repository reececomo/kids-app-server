<?php
/*
 * KidsAppServer
 *   Serves files for the kids application
 *   Provides an interface for result retrieval
 *
 * Author: Reece Como
 */

// GLOBAL Locations
DEFINE('CONFIG', '../config/');

$page  = isset($_GET['page']) ? $_GET['page'] : "index";
$error = "";

// GLOBAL Database variables
$db = parse_ini_file(CONFIG. "db.ini");
DEFINE("DATABASE_HOST", 	$db['host']);
DEFINE("DATABASE_NAME",		$db['name']);
DEFINE("DATABASE_USERNAME",	$db['user']);
DEFINE("DATABASE_PASSWORD",	$db['pass']);

$valid_pages = [
    'index',
    'login',  // Authentication
    'logout',
    'videos',
    'results',     // Results
    'results_download',
    'result_post',
    'researchers',
    'about',
    'apihelp'
];

// Library dependencies
require_once 'lib/orm/orm.config.php'; // Object Relational Mapping

// Other dependencies
require_once 'lib/kidsapp.HelperMethods.php';
require_once 'lib/kidsapp.Controllers.php';

// Assert database conneciton
if( ($database = \HubrORM\DatabaseConnection::connect()) === false )
    exit( 'Database is offline.' );

$auth = new Authentication();
if(logged_in() || $page == "login" || request_is_ajax()) {
	
} else
    set_page("login");

// Start output buffer
ob_start();
