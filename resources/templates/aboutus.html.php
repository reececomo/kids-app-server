<div class="container">

    <? include "part.navbar.php"; ?>

    <div class="row">
        <div class="col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0">
            <div class="whiteboard">
                <div class="row">
                    <div class="col-sm-12 aboutus">
                        <h1>Group K</h1>
                        <h2>Meet the brains behind the brawn...</h2>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="profile">
                            <div class="profile-img"><img src="/img/profiles/will.jpg"/></div>
                            <div class="caption">
                                <h3>William Owers</h3>
                                <h4>Project Manager &amp; Developer</h4>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="profile">
                            <div class="profile-img"><img src="/img/profiles/lauren.jpg"/></div>
                            <div class="caption">
                                <h3>Lauren Gee</h3>
                                <h4>QA Lead & Developer</h4>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="profile">
                            <div class="profile-img"><img src="/img/profiles/anton.jpg"/></div>
                            <div class="caption">
                                <h3>Anton Savill</h3>
                                <h4>Developer (Web App)</h4>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="profile">
                            <div class="profile-img"><img src="/img/profiles/mathias.jpg"/></div>
                            <div class="caption">
                                <h3>Mathias Gauber</h3>
                                <h4>Designer / Developer (UI &amp; UX)</h4>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="profile">
                            <div class="profile-img"><img src="/img/profiles/reece.jpg"/></div>
                            <div class="caption">
                                <h3>Reece Como</h3>
                                <h4>Developer (Back-end &amp; API)</h4>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="profile">
                            <div class="profile-img"><img src="/img/profiles/wilsen.jpg"/></div>
                            <div class="caption">
                                <h3>Wilsen Liu</h3>
                                <h4>Developer (Mobile Platforms)</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <? include 'part.footer.php'; ?>

</div>