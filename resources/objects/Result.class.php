<?php class Result extends ActiveRecord {

    static $TABLE = 'results';

    static $FIELDS = [
        'researcher_id',
        'security_code',
        'rank_1',
        'rank_2',
        'rank_3',
        'rank_4',
        'rank_5',
        'rank_6',
        'rank_7',
        'rank_8',
        'rank_9',
        'rank_10',
        'rank_11',
        'rank_12',
        'rank_13',
        'rank_14',
        'rank_15',
        'rank_16',
        'rank_17',
        'rank_18',
        'rank_19',
        'rank_20',
        'rank_21',
        'reflection'
    ];
}