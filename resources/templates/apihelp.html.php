<div class="container">

    <? include "part.navbar.php"; ?>

    <div class="row">
        <div class="col-xs-12">
            <div class="whiteboard">
                <a class="btn btn-default float-right" href="/">Go Back</a>
                <h3>API Settings </h3>
                <br/>
                <table class="table api-help">
                    <tr><th>Type</th><th>Action</th><th>URI</th><th>Parameters</th><th>Response</th></tr>
                    <tr>
                        <td rowspan="3"><b>Results</b></td>
                        <td>Submit</td>
                        <td>/result_post</td>
                        <td>
                            <ul>
                                <li>security_code <i>(Number, 4 Digit PIN)</i></li>
                                <li>researcher_id <i>(Database Entry Number)</i></li>
                                <li>rank_1 &rarr; rank_21</li>
                                <li>reflection <i>(Number, 1 - 3)</i></li>
                            </ul>
                        </td>
                        <td>
                            <h5><b>JSON Response:</b></h5>
                            <p>'Success' : '%DATABASE_ENTRY_ID'</p>
                            <p><i>or</i></p>
                            <p>'Error' : '%ERROR_MESSAGE'</p>
                        </td>
                    </tr>
                    <tr>
                        <td>Retrieve All (XLS)</td>
                        <td>/results_download</td>
                        <td></ul></td>
                        <td>
                            <h5><b>XLS Response:</b></h5>
                            <p><i>Spreadsheet File</i></p>
                        </td>
                    </tr>
                    <tr>
                        <td>Retrieve All (HTML)</td>
                        <td>/results_download</td>
                        <td><ul><li>preview = html</li></ul></td>
                        <td>
                            <h5><b>HTML Response:</b></h5>
                            <p><i>Results HTML Table</i></p>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <? include "part.footer.php"; ?>
</div>