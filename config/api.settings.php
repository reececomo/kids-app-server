<script type="text/javascript">

    /*
     * Configuration settings
     * for WebApp
     */
    var WebApp = {
        host : 'http://localhost:81/'                   /* Testing 1 */
        //host : 'http://localhost/'                    /* Testing 2 */
        //host : 'http://192.168.1.2/'                  /* Testing 3 */
        //host : 'http://kidsvalues.biz.uwa.edu.au/'    /* Production Server 1 */
    };

</script>