<?php
/*
 * Homepage handler
 */
function ctrl_index() {
    display_template("index"); // nothing fancy
}

/*
 * Login form handler
 *   and Logout page request handler.
 *
 */
function ctrl_login() {
    // If POST request
    if(isset($_POST['email'])) { // --> try to log in
        if(Authentication::login($_POST['email'], $_POST['password']))
            set_page("index"); // Success! Redirect!
        else
            set_error("Could not authenticate Username and Password"); // Display an error message
    }

    display_template("login"); // go to login page
}

function ctrl_logout() {
    Authentication::logout(); // logout the user
    set_page("login"); // go to login page
}

/*
 * Video Dashboard
 */
function ctrl_videos() {
    // Add logic in here
    display_template("videos");
}

/*
 * Results Dashboard
 */
function ctrl_results() {
    $sf = 0;  // Start From
    $ns = 25; // Number to Show

    extract($_GET,EXTR_OVERWRITE); // Receive SF and NS

    $results = Result::get([
        'start' => abs($sf),
        'limit' => abs($ns)
    ]);

    $total_results   = Result::count();            // REPLACE with a COUNT method
    $number_of_pages = ceil($total_results/$ns);
    $page            = $ns <= 0 ? '???' : ceil($sf/$ns) + 1;

    display_template("results", [
        'results'         => $results,
        'total_results'   => $total_results,
        'number_of_pages' => $number_of_pages,
        'start_from'    => abs($sf),
        'num_show'      => abs($ns),
        'page'          => abs($page)
    ]);
}

function ctrl_results_download() {
    ob_clean();
    download_file('results');
}

function ctrl_result_post() {
    set_ajax();

    extract($_GET);     // GET  request
    extract($_POST);    // POST request

    if(required_vars([$security_code, $researcher_email, $rank_1, $rank_21, $reflection])) {

        if($researcher_email == "")
            $researcher_id = NULL;
        else {
            if($researcher = Researcher::find("email=$researcher_email"))
                $researcher_id = $researcher->id;
            else
                $researcher_id = NULL;
        }

        $result = new Result();
        $result->security_code = $security_code;
        $result->researcher_id = $researcher_id;
        $result->reflection = $reflection;

        // Assign all 21 ranks (programatically)
        for($i = 1; $i <= 21; $i++) {
            $result->{"rank_" . $i} = ${"rank_" . $i};
        }

        if(($id = $result->save()) !== false)
               json_dump(['Success' => $id]);
        else json_error("Error saving to database");
    } else json_error("Incomplete Request");

}

function ctrl_respondent_new() {
    ob_clean();
}

function ctrl_researchers() {
    display_template("researchers");
}

function ctrl_apihelp() {
    display_template("apihelp");
}

function ctrl_about() {
    display_template("aboutus");
}
