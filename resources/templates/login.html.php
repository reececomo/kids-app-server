<div class="container vertical-center">
    <div class="panel login-box">
        <h2>Download The App</h2>
        <ol>
            <li>Follow the "Go To App Page" link below</li>
            <li>Tap on "Add to Home Screen"</li>
            <li>Open the App on any iOS device</li>
        </ol>
        <h2><a href="/quiz/start.html"><?=icon('arrow-right')?> Go To App Page</a></h2>
    </div>
    <div class="panel login-box">
        <h1>Admin Login</h1>
        <h2><?=icon('lock')?>&nbsp; Single Sign On</h2>

        <? if(!empty($error)) { ?>
            <p class="warning-block">Error: <?=$error?></p>
        <? } ?>

        <form action="?page=login" method="POST">
            <div class="input-group">
                <span class="input-group-addon addon-35" id="desc-un">Email</span>
                <input name="email" type="text" class="form-control" placeholder="e.g. michelle@uwa.edu.au" aria-describedby="desc-un">
            </div>
            <div class="input-group">
                <span class="input-group-addon addon-35" id="desc-pw">Password</span>
                <input name="password" type="password" class="form-control" placeholder="e.g. ***********" aria-describedby="desc-pw">
            </div>

            <div style="text-align:right;margin-top:25px;">
                <input class="btn btn-primary" type="submit" value="Log In"/>
            </div>

        </form>
    </div>
</div>