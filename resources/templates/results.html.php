<div class="container">
    <? include "part.navbar.php"; // Display Navigation Bar ?>

    <div class="row">
        <div class="col-lg-12">
            
            <div class="pg-content">
                <h1>Results <small><a class="btn btn-default float-right" href="results_download"><?=icon('download-alt')?> Download <b>All</b> as Spreadsheet</a></small></h1>

                <!-- Number of results -->
                <div class="centered">
                    <ul class="pagination">
                        <li class="<?=$page <= 1 ? 'disabled' : '';?>">
                            <a href="<?=$page <= 1 ? '#' : page_url('results',['ns'=>$num_show,'sf'=>$start_from-$num_show])?>" aria-label="Previous">
                                <span aria-hidden="true">&laquo; Prev</span>
                            </a>
                        </li>

                        <?

                        $d3_before = true;
                        $d3_after  = true;

                        for ($i = 1; $i <= $number_of_pages; $i++)
                        {
                            $first = $i == 1;
                            $last  = $i == $number_of_pages;
                            $within_3_pages = $i <= ($page + 3) && $i >= ($page - 3);

                            if($first || $last || $within_3_pages) {
                                // Iterator
                                $button = $page == $i ? $button = "active" : '';
                                $st_fr  = $num_show * ($i -1);
                                $link   = page_url("results",[
                                    'ns' => $num_show,
                                    'sf' => $st_fr
                                ]);

                                print "<li class='$button'><a href='$link'>$i</a></li>";

                            } else {
                                if($d3_before && $i < $page) {
                                    print "<li class='disabled'><a href='#'>...</a></li>";
                                    $d3_before = false;
                                } else if($d3_after && $i > $page) {
                                    print "<li class='disabled'><a href='#'>...</a></li>";
                                    $d3_after = false;
                                }
                            }

                        } ?>

                        <li class="<?=$page >= $number_of_pages ? 'disabled' : '';?>">
                            <a href="<?=$page >= $number_of_pages ? '#' : page_url('results',['ns'=>$num_show,'sf'=>$num_show+$start_from])?>" aria-label="Next">
                                <span aria-hidden="true">Next &raquo;</span>
                            </a>
                        </li>
                    </ul>
                    <p class="hint">Showing results <?=$start_from+1?> to <?= ($start_from + $num_show) < $total_results ? $start_from + $num_show : $total_results?> of <?=$total_results?><br/><br/></p>
                </div>
                
                <table class="table">  
                    <tr>
                        <th colspan='3'>Top 3 Videos</th>
                        <th colspan='15'>Middle 4 - 18 Videos</th>
                        <th colspan='3'>Bottom 3 Videos</th>
                        <th>Reflection Accuracy</th>
                    </tr>

                    <? if(empty($results)){ // No Results?>
                        <tr><td class="hint" colspan="22">No responses to display.</td></tr>

                    <? } else foreach($results as $result) { // Display results in this format ?>
                        <tr id="<?=$result->id?>">
                            <? for($i = 1; $i <= 21; $i++ ) {
                                if($i < 4) $rank = 'top';           // If top 3, style as 'top'
                                else if ($i > 18) $rank = 'bot';    // If bottom 3, style as 'bot'
                                else $rank = 'avg';                 // If middle 15, style as 'avg'
                                print "<td class='$rank'>".$result->{"rank_$i"}.'</td>';
                            } ?>
                            <td class='res-<?=$result->reflection?>'><div class="accuracy-bar"><?=$result->reflection?></div></td>
                        </tr>
		            <? } ?>
                </table>
            </div>
        
        </div>
    </div>
</div>
