<?php class Researcher extends ActiveRecord {

    static $TABLE = 'researchers';

    static $FIELDS = [
        'name',
        'email'
    ];
}